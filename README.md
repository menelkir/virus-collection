# Virus Collection

Collection of ancient computer virus source codes (mostly). There should be around 2000 files in this repository with a few being Pascal source codes or assembled COM files. A portion of the source codes are also result of reverse engineering.

The content of this repository is for educational purposes, I am not responsible for the misuse of it.

Source Repository: https://github.com/guitmz/virii
Malware Repository: https://github.com/Konnor88/malware-samples
Live Malware repository: https://github.com/ytisf/theZoo